const mysql = require('mysql');

const conn = mysql.createConnection({
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASSWORD || 'egresadosFisi',
  database: process.env.DB_NAME || 'SEG_EGRESADO',
  port: process.env.DB_PORT || 3306,
});

conn.connect();

module.exports = conn;
