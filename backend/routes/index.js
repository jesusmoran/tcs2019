var express = require('express');
var router = express.Router();
const db = require('../db');

router.get('/', (req, res, next) => {
  db.query('SELECT * FROM Persona', (error, results, fields) => {
    if(error) throw new Error(error);
    res.send({results});
  })
});

router.post('/', (req, res, next) => {
  const { name, lastNameP, lastNameM, add, doc, age } = req.body;

  db.query('INSERT INTO Persona(numDoc, apellidoPat, apellidoMat, nombres, edad) VALUES (?,?,?,?,?)', [
    doc, lastNameP, lastNameM, name, age
  ], (error, results, fields) => {
    if(error) throw new Error(error);
    res.send({results});
  });
});

module.exports = router;
