import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Home from './Componente/Home.js';
import ValorarServiciosUniversitarios from './Componente/ValorarServiciosUniversitarios.js';
import ValorarFormacionAcademica from './Componente/ValoreFormacionAcademica.js';

function App() {
    return (
        <Router>
            <Route path='/' exact component={Home}/>
            <Route path='/services' component={ValorarServiciosUniversitarios}/>
            <Route path='/formacion' component={ValorarFormacionAcademica}/>
        </Router>
    );
}

export default App;
