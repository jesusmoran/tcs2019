import React from 'react';
import '../App.css';

import { Link } from 'react-router-dom';

import fisi from '../img/logo-fisi.png';
import iconmenu from '../img/iconMenu.png';
import imgsection from '../img/sectionIMG.png'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={fisi} className="App-logo" alt="logo" />        
      </header>

      <nav className="princi">
        <ul id="primero">
            <li id="menu">
                <input type="checkbox" id="check" />
                <label for="check" className="icon-menu">
                    <img src={iconmenu} alt="iconmenu" />
                </label>
                <nav className="menu" id="meniu">
                        <ul>
                            <Link className='tile' to='/'>Inicio</Link>
                            <Link className='tile' to='/services'>Servicios</Link>
                            <Link className='tile' to='/formacion'>Formación</Link>
                            {/* <a href="/html/ActualizarDatosPersonales.html"><li>Actualizar Datos</li></a>
                            <a href="/html/RegistrarFormacionAcademica.html"><li>Registrar Formación Académica</li></a>
                            <a href="/html/RegistrarExperienciaLaboral.html"><li>Registrar Experiencia Laboral</li></a>
                            <a href="/html/ValorarFormacionAcademica.html"><li>Valorar Formación Académica</li></a>
                            <a href="/html/ValorarServiciosUniversitarios.html"><li>Valorar Servicios Universitarios</li></a> */}
                        </ul>
                </nav>
                
            </li>
            <a href="/html/index.html"><li id="inicio">Inicio</li></a>
            <a href="/html/index.html"><li id="bene">Beneficios</li></a>
            <a href="/html/index.html"><li id="serv">Servicios y Trámites</li></a>
            <a href="/html/index.html"><li id="noti">Noticias FISI</li></a>
            <a href="/html/index.html"><li id="revi">Revista FISI</li></a>
            <a href="/html/index.html"><li id="user">Usuario</li></a>
        </ul>
      </nav>
    

      <section>
      <div>
        <div className="row">
          <div className="col-xs-10">
            <h1 className="h1">
              Valore los Servicios Universitarios recibidos
            </h1>
          </div>
          
        </div>
        <div className="vista">
          <form>
            <br />
            <div id="a123">
              <h3 className="h3">Calidad del Servicio</h3>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Formacion integral</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Calidad Docente</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Calidad de Atención del Asesor de Tesis
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Calidad de Atención del Personal de la Oficina
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Calidad de Atención del Director
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Servicio del Trámite Administrativo
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
            </div>
            <div id="a123">
              <h3 className="h3">Calidad de los Recursos</h3>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Bibliotecas</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Bases de Datos Indexadas</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Apoyo para la Difusión de Resultados de Investigación
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Realización de Eventos Científicos
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
            </div>
            <div id="a123">
              <h3 className="h3">Calidad de las Instalaciones</h3>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Infraestructura</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Servicios Higiénicos</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Laboratorios</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Aulas</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Laboratorios de Investigación</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Estacionamiento</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
            </div>
            <div id="a123">
              <h3 className="h3">Vinculaciones Laborales</h3>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Inserción Laboral</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Vinculación con Instituciones Nacionales
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Vinculación con Instituciones Internacionales
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
            </div>
            <div id="a123">
              <h3 className="h3">Beneficios Estudiantiles</h3>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Seguro de Salud (Autoseguro)</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Movilidad Interna (Uso del Ómnibus Univ)
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Comedor Universitario</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">
                    Movilidad Estudiantil (Pasantías en Extranjero)
                  </label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 col-md-4">
                  <label className="item">Carné Universitario</label>
                </div>
                <div className="col-xs-2 col-md-8">
                  <input className="score" type="button" value="Excelente" />
                  <input className="score" type="button" value="Muy Bueno" />
                  <input className="score" type="button" value="Bueno" />
                  <input className="score" type="button" value="Regular" />
                  <input className="score" type="button" value="Pésimo" />
                  <input
                    className="score"
                    type="button"
                    value="No lo utilizo"
                  />
                  <input className="score" type="button" value="Desconocía" />
                </div>
              </div>
            </div>
          </form>

          <br />
          <hr />
        </div>
      </div>
    </section>
    </div>
  );
}

export default App;