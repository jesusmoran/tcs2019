import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import fisi from '../img/logo-fisi.png';
import iconmenu from '../img/iconMenu.png';
import imgsection from '../img/sectionIMG.png'

const App = () => (
    <div className="App">
        <header className="App-header">
            <img src={fisi} className="App-logo" alt="logo" />
        </header>

        <nav className="princi">
            <ul id="primero">
                <li id="menu">
                    <input type="checkbox" id="check" />
                    <label for="check" className="icon-menu">
                        <img src={iconmenu} alt="iconmenu" />
                    </label>
                    <nav className="menu" id="meniu">
                        <ul>
                            <Link className='tile' to='/'>Inicio</Link>
                            <Link className='tile' to='/services'>Servicios</Link>
                            <Link className='tile' to='/formacion'>Formación</Link>
                            {/* <a href="/html/ActualizarDatosPersonales.html"><li>Actualizar Datos</li></a>
                    <a href="/html/RegistrarFormacionAcademica.html"><li>Registrar Formación Académica</li></a>
                    <a href="/html/RegistrarExperienciaLaboral.html"><li>Registrar Experiencia Laboral</li></a>
                    <a href="/html/ValorarFormacionAcademica.html"><li>Valorar Formación Académica</li></a>
                    <a href="/./ValorarServiciosUniversitarios.html"><li>Valorar Servicios Universitarios</li></a> */}
                        </ul>
                    </nav>
                </li>
                <a href="/html/index.html"><li id="inicio">Inicio</li></a>
                <a href="/html/index.html"><li id="bene">Beneficios</li></a>
                <a href="/html/index.html"><li id="serv">Servicios y Trámites</li></a>
                <a href="/html/index.html"><li id="noti">Noticias FISI</li></a>
                <a href="/html/index.html"><li id="revi">Revista FISI</li></a>
                <a href="/html/index.html"><li id="user">Usuario</li></a>
            </ul>
        </nav>


        <section>
            <ul>
                <li>
                    <h1>Lorem Ipsum</h1>
                    <p>Nam turpis
                        tortor, gravida sit amet est vel,
                        suscipit convallis nibh. Integer
                        sodales venenatis erat in dapibus.
                        Donec quam lectus, porta consectetur
                est vitae, finibus tristique eros.</p>
                    <img src={imgsection} />
                </li>

                <li>
                    <h1>Lorem Ipsum</h1>
                    <p>Nam turpis
                        tortor, gravida sit amet est vel,
                        suscipit convallis nibh. Integer
                        sodales venenatis erat in dapibus.
                        Donec quam lectus, porta consectetur
                est vitae, finibus tristique eros.</p>
                    <img src={imgsection} />
                </li>

                <li>
                    <h1>Lorem Ipsum</h1>
                    <p>Nam turpis
                        tortor, gravida sit amet est vel,
                        suscipit convallis nibh. Integer
                        sodales venenatis erat in dapibus.
                        Donec quam lectus, porta consectetur
                est vitae, finibus tristique eros.</p>
                    <img src={imgsection} />
                </li>

            </ul>
        </section>
    </div>
);

export default App;