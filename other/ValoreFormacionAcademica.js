import React from 'react';
import './App.css';

import fisi from './img/logo-fisi.png';
import iconmenu from './img/iconMenu.png';
import imgsection from './img/sectionIMG.png'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={fisi} className="App-logo" alt="logo" />        
      </header>

      <nav className="princi">
        <ul id="primero">
            <li id="menu">
                <input type="checkbox" id="check" />
                <label for="check" className="icon-menu">
                    <img src={iconmenu} alt="iconmenu" />
                </label>
                <nav className="menu" id="meniu">
                        <ul>
                            <a href="/html/ActualizarDatosPersonales.html"><li>Actualizar Datos</li></a>
                            <a href="/html/RegistrarFormacionAcademica.html"><li>Registrar Formación Académica</li></a>
                            <a href="/html/RegistrarExperienciaLaboral.html"><li>Registrar Experiencia Laboral</li></a>
                            <a href="/html/ValorarFormacionAcademica.html"><li>Valorar Formación Académica</li></a>
                            <a href="/html/ValorarServiciosUniversitarios.html"><li>Valorar Servicios Universitarios</li></a>
                        </ul>
                </nav>
                
            </li>
            <a href="/html/index.html"><li id="inicio">Inicio</li></a>
            <a href="/html/index.html"><li id="bene">Beneficios</li></a>
            <a href="/html/index.html"><li id="serv">Servicios y Trámites</li></a>
            <a href="/html/index.html"><li id="noti">Noticias FISI</li></a>
            <a href="/html/index.html"><li id="revi">Revista FISI</li></a>
            <a href="/html/index.html"><li id="user">Usuario</li></a>
        </ul>
      </nav>
    

      <section>
        <ul>
             <div>
        <div className="row">
          <div className="col-xs-10">
            <h1 className="h1">Valore la Formacion Academica Recibida</h1>
          </div>
        </div>
        <div className="quest">
          <div className="preguntas">
            <h1 className="p1">
              De que manera los estudios de postgrado cursados ha influenciado
              en un desarrollo personal
            </h1>
            <h1 className="p2">
              Perfiles de egreso de los programas de Postgrado
            </h1>
            <h1 className="p3">Valore el perfil de egreso</h1>
            <h1 className="p4">Valore el plan de estudios cursado</h1>
          </div>

          <div className="respuesta">
            <select className="r1">
              <option value="" disabled selected>
                Seleccione una opcion
              </option>
              <option value="1">A sido promovido en el institucion</option>
              <option value="2">Incremento su salario</option>
              <option value="3">
                Aperturo nuevas oportunidades laborales mejor remuneradas
              </option>
              <option value="4">No logro cambios significativos</option>
            </select>
            <select className="r2">
              <option value="" disabled selected>
                Seleccione una opcion
              </option>
              /aqui irian las imagenes si tuviera una >:v
            </select>
            <input className="r3" />
            <input className="r4" />
          </div>
        </div>
      </div>
        </ul>
    </section>
    </div>
  );
}

export default App;