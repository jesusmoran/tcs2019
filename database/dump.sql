create database SEG_EGRESADO;
use SEG_EGRESADO;

create table Persona (
personaID       int AUTO_INCREMENT,
numDoc       	varchar(12)  null,
edad        	smallint      null,
apellidoPat     varchar(20)  null,
apellidoMat     varchar(20)  null,
nombres        	varchar(20)	 null,
ubigeoNac       varchar(30)  null,
ultimoRegistro  varchar(30)  null,
domicilioActual varchar(30)  null,
distrito        varchar(30)  null,
telefFijo       varchar(11)  null,
telefCelular    varchar(9)   null,
correoPersonal  varchar(50)  null,
correoLaboral   varchar(50)  null,
fechaNac        date         null,
fechaRegistro   date         null,
constraint PK_PERSONA primary key (personaID)
);

create unique index PERSONA_PK on Persona (
personaID
);

insert into Persona (personaID, numDoc, edad, apellidoPat, apellidoMat, nombres) values (001, '71238397', 23, 'Mori', 'Rojas', 'Vicente Omar');


create table Experiencia_Laboral (
ExpLaboral_ID     smallint   not null,
fechaInicio       date  	 null,
fechaFin       	  date       null,
fechaRegistro     date       null,
institucion       varchar(50)  null,
tiempoServicio     varchar(20)  null,
relacionFormacion  varchar(20)	 null,
funcionesPrin       varchar(20)  null,
tiempoDemEmpleo     varchar(20)  null,
contrapMensual    smallint  null,
numEmpleo         smallint  null,
nivelSatisfaccion smallint  null,
constraint PK_Experiencia_Laboral primary key (ExpLaboral_ID)
);

create unique index Experiencia_Laboral_PK on Experiencia_Laboral (
ExpLaboral_ID
);

insert into Experiencia_Laboral (ExpLaboral_ID, fechaInicio) values (01, '2019-01-10');

create table Formacion_Academica (
codAlumno            char(8)   not null,
programaCursado      varchar(50)   null,
sitAcadAct       	 varchar(50)   null,
fechaIngreso         date      null,
fechaEgreso          date      null,
fechaExpGrado        date      null,
fechaExpTitulo       date	   null,
fechaCertificacion   date      null,
meritoObtenido       smallint  null,
numHorasLectivas     int       null,
constraint PK_Formacion_Academica primary key (codAlumno)
);

create unique index Formacion_Academica_PK on Experiencia_Laboral (
codAlumno
);

insert into Formacion_Academica (codAlumno, sitAcadAct) values ('15200054', 'Activo');

create table Perfil_Egreso_Valoracion (
perfilEgresoID       int   not null,
fechaRegistro        date  null,
estado       	     int   null,
constraint PK_Perfil_Egreso_Valoracion primary key (perfilEgresoID)
);

create unique index Perfil_Egreso_Valoracion_PK on Perfil_Egreso_Valoracion (
perfilEgresoID
);

insert into Perfil_Egreso_Valoracion values ('101', '2019-01-01', 1);

create table Servicio_Universitario_Valoracion (
servUniValID       int   not null,
fechaRegistro        date  null,
estado       	     int   null,
constraint PK_Servicio_Universitario_Valoracion primary key (servUniValID)
);

create unique index Servicio_Universitario_Valoracion_PK on Perfil_Egreso_Valoracion (
servUniValID
);

insert into Servicio_Universitario_Valoracion values ('101', '2019-01-01', 1);